# Print current dir
import os
print(os.getcwd())

# Ask user to input file path
FILE_PATH = input('Input your file path: ').strip()
list_ip = []


def check_existed_ip(given_ip):
    for temp_ip in list_ip:
        if str(temp_ip).strip() == given_ip:
            return True
    return False


count_line = 0
with open(FILE_PATH, 'r') as file_open:
    for line in file_open:
        count_line = count_line + 1
        try:
            string_ip = line[0: line.index('- -')].strip()
            if not check_existed_ip(string_ip):
                list_ip.append(string_ip)
        except:
            print('Exception at line %s' % str(count_line))
            print(line)
print('List IP in file:')
for ip in list_ip:
    print('\t%s' % ip)
